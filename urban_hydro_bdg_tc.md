# Bantaran Cikapundung: air sungai dan air tanah yang saling berhubungan

Oleh Dasapta Erwin Irawan ([ORCID](https://orcid.org/0000-0002-1526-0863))

> Saya biasa menjerang air sampai mendidih pak. itu didikan keluarga saya... rasanya itu belum pernah saya dengan di sekolah? Juga pas melihat air, saya lihat warna, baru dan rasanya, untuk melihat apakah air itu bisa diminum atau tidak??

## Cikapundung dalam ruang

Itu ada salah satu cuplikan hasil riset tahun 2018 yang kami, dari Kelompok Keilmuan Geologi Terapan, Fakultas Ilmu dan Teknologi Kebumian, Institut Teknologi Bandung, kerjakan. Sejak 2001, saya memimpin Tim Riset yang fokus mengkaji ilmu hidrogeologi di kawasan urban (*urban hydrogeology*) dan kawasan gunungapi (*volcanic hydrogeology*), khususnya kualitas air tanah serta interaksinya dengan air permukaan.

Sungai Cikapundung adalah satu saja dari ?sekian? sungai yang mengalir melalui Kota Bandung. Mengalir dari utara di kawasan pegunungan Tangkuban Parahu, ke arah selatan hingga bertemu dengan Sungai Citarum. Aliran sungainya sepanjang 15,5 km, dengan luas Daerah aliran sungainya (DAS) seluas 14,9 Hektar. Karena cukup luas, maka penataan ruangnya melibatkan dua kota dan dua kabupaten: Kota Bandung, Kota Cimahi, Kabupaten Bandung Barat, dan Kabupaten Bandung. Di sinilah mestinya komunikasi berperan penting.

## Cikapundung dalam waktu

Kurang lebih 20 tahun lalu, grup kami menelaah perilaku air sungainya serta hubungannya dengan air tanah di sekitarnya. Adalah Rachmat Fajar Lubis dan Prof. Deny Juanda Puradimaja, yang memulai kegiatan pemetaan aliran air tanah dan air sungai di tahun 1997, yang menemukan bahwa air S. Cikapundung dan air tanah di bantarannya saling berhubungan. Kualitas air sungai akan mempengaruhi air tanah dan begitu pula sebaliknya. Beberapa penulis lainnya juga terdeteksi telah mendalami interaksi antara kedua badan air itu secara numerik,, analisis kesetimbangan debit air sungai,, analisis kewilayahan dan partisipasi publik, analisis kandungan logam berat,, analisis kandungan mikroorganisme,, dan analisis vegetasi.

Secara keseluruhan ada hasil penting dari berbagai hasil riset tersebut adalah bahwa air S. Cikapundung yang debitnya cukup besar, namun kualitasnya buruk. Air telah tercemar dengan sumber polutan tidak hanya dari industri, tetapi juga dari rumah tangga (domestik). Keduanya merupakan dampak yang berasal dari aktivitas manusia (antropogenik), yang dikonfirmasi pula dari hasil pengukuran isotop S34 oleh Batubara dan Iskandar (2018) yang baru diselesaikan dalam format tesis magister ITB. Karena air sungai dan air tanah saling terhubung, maka bila air sungai kotor, maka air tanahnya pun akan kotor. Begitu pula sebaliknya.

Insert foto (gambar 1)

Foto di atas adalah tipikal titik pembuangan limbah rumah tangga yang langsung dibuang ke selokan yang berakhir di S. Cikapundung, tanpa diolah atau diendapkan terlebih dahulu. Bayangkan kalau ini dilakukan oleh sebagian besar rumah di Bandung.

Upaya kami berikutnya adalah memanfaatkan teknik pengambilan data secara time series untuk menampilkan fluktuasi komponen kimiawi yang ada dalam air sungai dan air tanah pada kurun waktu tertentu, yang menampakkan beberapa hasil menarik. Pengukuran TDS (total dissolved solids) atau sering disetarakan dengan kadar garam, di tiga lokasi di Bandung Utara dan Cimahi pada tahun 2017 menunjukkan adanya peningkatan di akhir minggu. Apakah ini berhubungan dengan melonjaknya pengunjung Kota Bandung di akhir minggu yang berujung kepada tingginya hunian hotel dan penginapan di daerah tersebut? Kami belum tahu pasti. 

Pengukuran berikutnya pada waktu dan lokasi yang berbeda, menunjukkan adanya peningkatan nilai TDS dan BOD menuju ke hilir. Polanya adalah tinggi di pagi hari dan makin rendah di sore hari. Hal ini menunjukkan bahwa aktivitas pembuangan limbah terbanyak mungkin terjadi di pagi hari, yang mungkin saja merupakan akumulasi sejak senja hari saat warga mulai banyak menghabiskan waktu di rumah. Peningkatan nilai indikator kualitas air ke arah hulu dapat dijadikan barometer sejauh mana tingkat polusi, kecepatannya dan jaraknya dari arah hulu. Nilai BOD (Biological Oxygen Demand) sangat meningkat dua kali lipat ke arah hilir diikuti dengan kenaikan populasi bakteri E.coli hingga lima kali lipat. Dari sini dapat dilihat bahwa tidak hanya dari parameter fisik (TDS), tetapi parameter organik juga mengindikasikan penurunan kualitas. Sebagai catatan, tata guna lahan kawasan di sekitar S. Cikapundung berubah dari pemukiman, ke arah industri dan perdagangan ke arah hilir.  

Yang mengkhawatirkan adalah nilai-nilai tersebut tidak berbeda banyak untuk air tanah di sumur warga. Hasil wawancara singkat dengan beberapa warga di area Cihampelas menunjukkan bahwa, bila dibandingkan dengan era 80?an, lebih dari separuh sumur milik warga (dari kurang lebih 50 KK), telah ditutup oleh pemiliknya, karena tidak dapat dikonsumsi lagi, bahkan untuk mandi dan cuci. Mereka kemudian beralih ke layanan PDAM Kota Bandung yang tidak mengalir secara penuh dalam sehari. Dari sini saja bisa kita lihat bahwa rujukan warna, bau dan rasa saja tidak dapat digunakan untuk menentukan apakah air dapat dikonsumsi atau tidak. Masyarakat perlu menguji kandungan air di rumahnya masing-masing untuk mengetahui apakah air dapat dikonsumsi. 

## Sedikit menyinggung kebijakan

Polusi oleh limbah domestik saat ini masuk luput dari penanganan para pengelola daerah. Mayoritas regulasi pencegah pencemaran baru menembak limbah industri, yang mana juga sangat penting mengingat kasus pencemaran makin banyak terungkap di wilayah Cekungan Bandung setelah TNI AD turut berpartisipasi sebagai pengawas dan eksekutor,. 

Untuk limbah domestik sangat penting regulasi untuk memastikan bahwa permukan atau suatu kawasan tidak membuang limbahnya langsung ke sungai. Regulasi itu perlu juga didukung dengan upaya pemerintah daerah untuk menyediakan infrastruktur pengolah limbah air kotor, yang mana ini biasanya sulit karena hambatan dana. Oleh karena itu perlu juga banyak pendidikan kepada masyarakat tentang membangun bak pengolah limbah domestik sederhana. Berbagai teknologi sederhana dan murah dapat diadopsi, sejalan dengan perlunya ditumbuhkan minat para pembuat kebijakan untuk mencari informasi  tersebut secara daring. Saat ini mencari hasil riset secara daring adalah cara termudah. 

Di sisi lain, sebagai penulis/peneliti perlu ditumbuhkan kesadaran untuk mengarsipkan karyanya secara daring agar mudah ditemukan oleh berbagai pihak, termasuk pembuat kebijakan. Saat ini pengarsipan daring ini juga sudah sangat mudah dengan banyaknya repositori umum yang gratis dan terbuka. Namun demikian, saya merekomendasikan untuk mengunggah hasil riset di Repositori Institusi perguruan tinggi masing-masing.
