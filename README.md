# Urban hidrologi Bandung

This repository holds files related to my manuscript to sent to The Conversation Indonesia, based on series of research results from our research group.

Contributor:
- Dasapta Erwin Irawan
- Prof. Deny Juanda Puradimaja
- Achmad Darul
- Dimas Maulana Wibowo 
- Rahman Hakim
- Rakha 
- Ulfa

Open `Urban_hydrology_bdg_tc.pdf` to read the pdf version or the md version (will be updated soon) `urban_hydro_bdg_tc.md`. 